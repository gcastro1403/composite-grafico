
public class RunComposite {

	public static void main(String[] args) {
		
		Grafico circulo = new Circulo(10f);
		Grafico cuadrado = new Cuadrado(20f);
		Grafico triangulo = new Triangulo(5f,3f);
		
		GrupoDeImagenes grupoDeImagenes = new GrupoDeImagenes();
		
		grupoDeImagenes.addGrafico(circulo);
		grupoDeImagenes.addGrafico(cuadrado);
		grupoDeImagenes.addGrafico(triangulo);
		
		grupoDeImagenes.pintar();
		grupoDeImagenes.area();
		
		grupoDeImagenes.removeGrafico(circulo);
		
		System.out.println("\npintando despues del remove");
		
		grupoDeImagenes.pintar();
		
		
	}

}
