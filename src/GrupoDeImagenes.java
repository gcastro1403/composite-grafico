import java.util.ArrayList;
import java.util.List;

public class GrupoDeImagenes implements Grafico{
	
	private List<Grafico> childGraficos;
	
	public GrupoDeImagenes() {
		
		this.childGraficos = new ArrayList<>();
	}
	
	
	@Override
	public void pintar() {
		
		for(Grafico forma: this.childGraficos){
			forma.pintar();
        }
		
	}
	
	public void addGrafico(Grafico forma) {
		
		this.childGraficos.add(forma);
		
	}
	
	public void removeGrafico (Grafico forma) {
		
		this.childGraficos.remove(forma);
		
	}


	@Override
	public void area() {
		
		for(Grafico forma: this.childGraficos){
			forma.area();
        }
		
		
	}
	
	
}
