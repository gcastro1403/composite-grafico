
public class Cuadrado implements Grafico{
	
	private Float lado;
	
	public Cuadrado (Float lado) {
		this.lado = lado;
	}

	@Override
	public void pintar() {
		System.out.println("Se esta pintando un cuadrado con lado " + this.lado);
		
	}
	
	@Override
	public void area() {
		
		float area = this.lado * this.lado;
		
		System.out.println(" El area de el cuadrado es : " + area);
	}
	
	
}
