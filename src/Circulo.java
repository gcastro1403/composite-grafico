
public class Circulo implements Grafico{
	
	private Float radio;
	
	public Circulo (Float radio) {
		this.radio = radio;
	}
	
	
	@Override
	public void pintar() {
		
		System.out.println("Se esta pintando un circulo con radio de : " + this.radio);
		
	}


	@Override
	public void area() {
		
		double area = Math.PI * Math.pow(radio,2);
		
		System.out.println("El area del circulo es " + area);
	}
	
	
}
