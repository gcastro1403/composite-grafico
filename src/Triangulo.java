
public class Triangulo implements Grafico{
	
	private Float base;
	private Float altura;
	
	public Triangulo(Float base, Float altura) {
		this.base = base;
		this.altura = altura;
	}
	
	@Override
	public void pintar() {
		System.out.println("Se esta pintando un triangulo con base " + this.base + " y altura " + this.altura);
		
	}

	@Override
	public void area() {
		float area = this.base * this.altura;
		
		System.out.println("El area de el circulo es " + area);
		
	}
	
}
